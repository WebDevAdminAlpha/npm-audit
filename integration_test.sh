#!/bin/sh

find "${CI_PROJECT_DIR}/test/fixtures" -type d -mindepth 1 -maxdepth 1 | while read -r project; do
    REPORT_PATH="$project/$REPORT_FILENAME"
    DIRNAME="$(basename $project)"
    TOOL="$(echo $DIRNAME | cut -d '-' -f 1)"
    EXPECTATION="${CI_PROJECT_DIR}/test/expect/${DIRNAME}.txt"

    echo "running on $project with $TOOL audit"
    
    docker run --volume "$project:/tmp/project" \
        --env CI_PROJECT_DIR=/tmp/project \
        --env TOOL="$TOOL" $TMP_IMAGE /analyzer run

    echo "comparing generated report with $EXPECTATION ..."

    # Extract findings from report
    jq -r ".vulnerabilities[] | .location.dependency.package.name" \
        "$REPORT_PATH" | sort | uniq > /tmp/findings.txt

    cmp /tmp/findings.txt "$EXPECTATION" || {
        diff "$REPORT_PATH" /tmp/findings.txt
        exit 1
    }

    echo "... ok"
done

exit 0
