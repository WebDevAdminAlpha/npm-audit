# npm-audit analyzer (npm|yarn) audit changelog

## v1.3.0
Infer version in Docker build

## v1.2.0
Addition of integration tests and documentation.

## v1.1.0
Use Node 14 (LTS version)

## v1.0.0
Initial release
