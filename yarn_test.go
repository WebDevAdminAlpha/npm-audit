package main

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/gitlab/security-products/tests")
}

func TestConvertYarn(t *testing.T) {
	prependPath := "app"
	category := issue.CategoryDependencyScanning
	scanner := issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	var tcs = []struct {
		name                   string
		reportNodeModulesPaths bool
		input                  string
		want                   *issue.Report
	}{

		{
			name:                   "report node_modules paths",
			reportNodeModulesPaths: true,
			input: `
{"type":"auditAdvisory","data":{"resolution":{"id":996,"path":"","dev":false,"optional":false,"bundled":false},"advisory":{"findings":[{"version":"3.0.1","paths":["browser-env>window>jsdom>request>extend","jsdom>request>extend","coveralls>request>extend","@nuxtjs/icon>jimp>request>extend"]}],"id":996,"created":"2019-06-19T00:18:20.635Z","updated":"2019-07-24T18:05:41.047Z","deleted":null,"title":"Prototype Pollution","found_by":{"link":"","name":"asgerf"},"reported_by":{"link":"","name":"asgerf"},"module_name":"extend","cves":[],"vulnerable_versions":"<2.0.2 || >=3.0.0 <3.0.2","patched_versions":">=2.0.2 <3.0.0 || >=3.0.2","overview":"","recommendation":"","references":"- [HackerOne Report](https://hackerone.com/reports/381185)","access":"public","severity":"moderate","cwe":"CWE-471","metadata":{"module_type":"","exploitability":4,"affected_components":""},"url":"https://npmjs.com/advisories/996"}}}
`,
			want: &issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						Category:   category,
						Scanner:    scanner,
						Name:       "Prototype Pollution",
						Message:    "Prototype Pollution in extend",
						CompareKey: "app/package.json:extend:npm-audit:996",
						Severity:   issue.SeverityLevelMedium,
						Location: issue.Location{
							File: "app/package.json",
							Dependency: &issue.Dependency{
								Package: issue.Package{
									Name: "extend",
								},
								Version: "3.0.1",
							},
						},
						Identifiers: []issue.Identifier{
							{
								Type:  "npm-audit",
								Name:  "NPM-996",
								Value: "996",
								URL:   "https://npmjs.com/advisories/996",
							},
						},
						Links: []issue.Link{
							{
								URL: "https://npmjs.com/advisories/996",
							},
						},
					},
				},
				Remediations: []issue.Remediation{},
				DependencyFiles: []issue.DependencyFile{
					{
						Path:           "app/package.json",
						PackageManager: "yarn",
						Dependencies: []issue.Dependency{
							{
								Package: issue.Package{
									Name: "extend",
								},
								Version: "3.0.1",
							},
						},
					},
				},
			},
		},
	}

	pm := yarnPackageManager{}
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {

			if tc.reportNodeModulesPaths {
				os.Setenv("REPORT_NODE_MODULES_PATHS", "")
			} else {
				os.Unsetenv("REPORT_NODE_MODULES_PATHS")
			}

			r := strings.NewReader(tc.input)
			got, err := convertReport(pm, r, prependPath)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(tc.want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tc.want, got)
			}
		})
	}
}
