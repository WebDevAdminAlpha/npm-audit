package main

import (
	"bufio"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// YarnAdvisory represents an advisory produced by yarn audit
type YarnAdvisory struct {
	Type string
	Data struct {
		Advisory Advisory
	}
}

func toStandardReport(yarnAdvisories []YarnAdvisory) *AuditReport {
	var advisoryMap = make(map[string]Advisory)
	for _, advisory := range yarnAdvisories {
		advisoryMap[strconv.Itoa(advisory.Data.Advisory.ID)] = advisory.Data.Advisory
	}
	ret := new(AuditReport)
	ret.Advisories = advisoryMap
	return ret
}

// Decode is required to cope with the json-line format that is produces by yarn audit --json
func Decode(r io.Reader) ([]YarnAdvisory, error) {
	var yarnAdvisories []YarnAdvisory
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		var result YarnAdvisory
		item := scanner.Bytes()
		err := json.Unmarshal(item, &result)
		if err != nil || result.Type != "auditAdvisory" {
			continue
		}
		yarnAdvisories = append(yarnAdvisories, result)
	}
	if err := scanner.Err(); err != nil {
		return yarnAdvisories, err
	}

	return yarnAdvisories, nil
}

type yarnPackageManager struct{}

func (pm yarnPackageManager) name() issue.PackageManager {
	return issue.PackageManagerYarn
}

func (pm yarnPackageManager) build(path string) error {
	cmd := exec.Command("yarn", "install", "--ignore-engines")

	// if a lockfile is present, we use it
	// yarn requires package.json to be present, too
	if pm.lockfilePresent(path) && pathExists(filepath.Join(path, npmDepFile)) {
		log.Info("lockfile detected: run yarn ci")
		cmd = exec.Command("yarn", "install", "--ignore-engines", "--frozen-lockfile")
	}

	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (pm yarnPackageManager) analyze(path string) ([]byte, error) {
	cmd := exec.Command("yarn", "audit", "--json")
	cmd.Dir = path
	cmd.Env = os.Environ()
	return cmd.Output()
}

func (pm yarnPackageManager) lockfile() string {
	return yarnLockFile
}

func (pm yarnPackageManager) lockfilePresent(path string) bool {
	return pathExists(filepath.Join(path, pm.lockfile()))
}

func (pm yarnPackageManager) prepare(reader io.Reader) (*AuditReport, error) {
	yarnAdvisories, err := Decode(reader)
	if err != nil {
		return nil, err
	}

	return toStandardReport(yarnAdvisories), err
}
