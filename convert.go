package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"

	"github.com/logrusorgru/aurora"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// pm captures common functionality across different package managers
type pm interface {
	name() issue.PackageManager
	lockfile() string
	lockfilePresent(path string) bool
	build(path string) error
	analyze(path string) ([]byte, error)
	prepare(reader io.Reader) (*AuditReport, error)
}

var scanner = issue.Scanner{
	ID:   scannerID,
	Name: scannerName,
}

func (adv Advisory) identifiers() []issue.Identifier {
	ids := []issue.Identifier{}

	// NPM id is going to be the primary identifier
	// https://docs.gitlab.com/ee/development/integrations/secure.html#identifiers
	// note that adv.ID are always set

	ids = append(ids, issue.Identifier{
		Type:  scannerID,
		Name:  fmt.Sprintf("NPM-%d", adv.ID),
		Value: fmt.Sprintf("%d", adv.ID),
		URL:   fmt.Sprintf("https://npmjs.com/advisories/%v", adv.ID),
	})

	cves := []issue.Identifier{}
	//  extract CVE ids when available
	if len(adv.Cves) > 0 {
		for _, cve := range adv.Cves {
			cves = append(cves, issue.CVEIdentifier(cve.(string)))
		}
		// guarantees that the CVEs are always ordered the same way
		// so that the output, i.e., the JSON file, is deterministic
		// this is important to ensure that the QA tests do not fail
		// due to a JSON file with entries that have a different order
		// as compared to the test expectation
		sort.Slice(cves, func(i, j int) bool {
			return cves[i].Value < cves[j].Value
		})
	}

	return append(ids, cves...)
}

func getPm(path string) (pm, error) {

	toolEnv := os.Getenv(toolEnv)
	if toolEnv == "yarn" {
		return yarnPackageManager{}, nil
	}

	return npmPackageManager{}, nil
}

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	if canConnectToNpm() != nil {
		fmt.Println(aurora.Red("convert: cannot connect to NPM"))
		emptyReport := issue.NewReport()
		// fail gracefully: return an empty report
		return &emptyReport, nil
	}

	pm, err := getPm(prependPath)
	if err != nil {
		return nil, err
	}

	return convertReport(pm, reader, prependPath)
}

func convertReport(pmgr pm, reader io.Reader, prependPath string) (*issue.Report, error) {

	intermediateReport, err := pmgr.prepare(reader)

	if err != nil {
		return nil, err
	}

	// The output of npm-audit contains absolute paths (though not always),
	// so the absolute path of the root directory is needed
	// in order to convert absolute paths to relative paths.
	root := os.Getenv("ANALYZER_TARGET_DIR")
	if root == "" {
		root = os.Getenv("CI_PROJECT_DIR")
	}

	// convert output
	var issues []issue.Issue
	var dependencies = make(map[string]issue.Dependency)

	var advisories []Advisory

	for _, advisory := range intermediateReport.Advisories {
		advisories = append(advisories, advisory)
	}

	// sort dependency files by path
	sort.Slice(advisories, func(i, j int) bool {
		return advisories[i].ID < advisories[j].ID
	})

	depFile := os.Getenv(NpmAuditLockfileEnv)
	if depFile == "" {
		// this file has to be present for both yarn and npm
		depFile = npmDepFile
	}

	for _, advisory := range advisories {
		for _, findings := range advisory.Findings {
			dep := issue.Dependency{
				Package: issue.Package{
					Name: advisory.ModuleName,
				},
				Version: findings.Version,
			}
			_, ok := dependencies[dep.Name+dep.Version]
			if !ok {
				dependencies[dep.Name+dep.Version] = dep
				vuln := issue.DependencyScanningVulnerability{
					issue.Issue{
						Category: issue.CategoryDependencyScanning,
						Scanner:  scanner,
						Name:     advisory.Title,
						Severity: parseSeverityLevel(advisory.Severity),
						Location: issue.Location{
							File:       filepath.Join(prependPath, depFile),
							Dependency: &dep,
						},
						Identifiers: advisory.identifiers(),
						Links:       issue.NewLinks(advisory.URL),
					},
				}
				issues = append(issues, vuln.ToIssue())
			}
		}
	}

	var deps []issue.Dependency
	for _, v := range dependencies {
		deps = append(deps, v)
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	report.DependencyFiles = []issue.DependencyFile{
		{
			Path:           filepath.Join(prependPath, depFile),
			PackageManager: pmgr.name(),
			Dependencies:   deps,
		},
	}

	report.Sort()
	return &report, nil
}
