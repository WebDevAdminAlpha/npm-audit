module gitlab.com/gitlab-org/security-products/analyzers/npm-audit

require (
	github.com/logrusorgru/aurora v0.0.0-20181002194514-a7b3b318ed4e
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.22.0
)

go 1.13
