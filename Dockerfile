FROM golang:1.14.2-alpine3.11 AS builder
COPY . /build
WORKDIR /build
RUN go build -o /analyzer

RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM node:14-alpine
# install git for project with git-sourced dependencies
RUN apk add --no-cache git

# temporary workaround for
# https://github.com/nodejs/docker-node/issues/813#issuecomment-407339011
RUN npm config set unsafe-perm true

COPY --from=builder /analyzer /
WORKDIR "/"
ENTRYPOINT []
CMD ["/analyzer", "run"]
